const assert = require('assert')
const debug = require('debug')('fromXLSX')
const XSLX = require('xlsx')

function fromSheet (name, i, sheet) {
  debug(name)

  let valid = true
  let content = XSLX.utils.sheet_to_json(sheet, { raw: false, header: 'A', defval: null })

  if (content.length === 0) { return 0 }

  let row1 = content[0]
  // Object.keys(row1).forEach(key => {
  //   if (key.indexOf('__EMPTY') >= 0) {
  //     // todo error handling
  //     valid = false
  //   }
  // })
  if (row1['A'] == null) {
    valid = false
  }
  if (!valid) { return null }

  let columns = {}
  Object.keys(row1).forEach(key => {
    if (row1[key] !== null) { columns[key] = row1[key] }
  })
  // debug('Columns')
  // debug(columns)

  return function * () {
    let i = 0
    while (i < content.length) {
      if (i === 0) {
        i++
        continue
      }

      let row = {}
      Object.keys(columns).forEach(col => {
          row[columns[col]] = content[i][col]
      })
      let res = {
        table: name,
        row
      }
      debug(res)
      yield res
      i++
    }
  }
}

function fromXLSX (file) {
  let workbook = XSLX.readFile(file)
  let generators = []
  workbook.SheetNames.forEach((name, i) => {
    let g = fromSheet(name, i, workbook.Sheets[name])
    if (g) {
      generators.push(g)
    }
  })
  let generator = function * () {
    for (var i = 0; i < generators.length; i++) {
      let next = null
      let g = generators[i]()
      debug(`Generator ${i}`)
      while (!(next = g.next()).done) {
        yield next.value
      }
    }
  }
  return {
    generator,
    generators
  }
}

module.exports = {
  fromXLSX
}
