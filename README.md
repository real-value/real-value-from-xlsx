# real-value-from-xlsx Library     

## About

This library produces generators from an XLSX file.

## Install

```
npm install real-value-from-xlsx
yarn install real-value-from-xlsx
```

## How to use

```
import { fromXLSX } from 'real-value-lang-xlsx'
let {generator,generators} = fromXLSX('./test/Sample1.xlsx')
```

The return are
- generator : a generator over all rows
- generators: an array of generators over each sheet

