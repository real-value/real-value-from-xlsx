import { describe } from 'riteway'
import { fromXLSX } from '../index.js'
const debug = require('debug')('test')

describe.skip('Process Sample 1', async (assert) => {
  let { generators } = fromXLSX('./test/Sample1.xlsx')

  let gen1 = generators[0]()
  let rows = []
  for await (const rec of gen1) {
      debug(rec)
    rows.push(rec)
  }

  assert({
    given: 'Sample File',
    should: 'Produce generator',
    actual: generators.length,
    expected: 4
  })
})

describe('Process Test', async (assert) => {
  let { generator } = fromXLSX('./test/Test.xlsx')

  let rows = []
  for await (const rec of generator()) {
      debug(rec)
    rows.push(rec)
  }

  assert({
    given: 'Sample File',
    should: 'product correct number of rows',
    actual: rows.length,
    expected: 4
  })
})
